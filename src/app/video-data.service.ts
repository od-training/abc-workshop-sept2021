import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Video } from './dashboard/types';

const apiUrl = 'https://api.angularbootcamp.com';

@Injectable({
  providedIn: 'root',
})
export class VideoDataService {
  constructor(private httpClient: HttpClient) {}

  loadVideos(query?: string): Observable<Video[]> {
    const params = query ? { q: query } : undefined;

    return this.httpClient.get<Video[]>(`${apiUrl}/videos`, {
      params,
    });
  }

  loadVideo(videoId: string): Observable<Video> {
    return this.httpClient.get<Video>(apiUrl + '/videos/' + videoId);
  }
}
