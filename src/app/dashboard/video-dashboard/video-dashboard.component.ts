import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';

import { VideoDataService } from '../../video-data.service';

import { Video } from '../types';

export const videoListFilterQueryParam = 'videosFilter';
export const selectedVideoIdQueryParam = 'videoId';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss'],
})
export class VideoDashboardComponent {
  videoList: Observable<Video[]>;

  selectedVideo: Observable<Video | undefined>;

  constructor(
    private vds: VideoDataService,
    private router: Router,
    route: ActivatedRoute
  ) {
    this.videoList = route.queryParamMap.pipe(
      map((paramMap) => paramMap.get(videoListFilterQueryParam)),
      distinctUntilChanged(),
      switchMap((query) => this.vds.loadVideos(query ?? undefined))
    );

    this.selectedVideo = route.queryParamMap.pipe(
      map((paramMap) => paramMap.get(selectedVideoIdQueryParam)),
      distinctUntilChanged(),
      switchMap((videoId) => {
        if (videoId) {
          return this.vds.loadVideo(videoId);
        } else {
          return of(undefined);
        }
      })
    );
  }

  chooseVideo(video: Video) {
    this.router.navigate([], {
      queryParams: {
        [selectedVideoIdQueryParam]: video.id,
      },
      queryParamsHandling: 'merge',
    });
  }
}
