import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { videoListFilterQueryParam } from '../video-dashboard/video-dashboard.component';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.scss'],
})
export class StatFiltersComponent {
  videoFilterControl = new FormControl('');

  constructor(private router: Router, route: ActivatedRoute) {
    const initialFilterValue = route.snapshot.queryParamMap.get(
      videoListFilterQueryParam
    );
    this.videoFilterControl.patchValue(initialFilterValue);

    this.videoFilterControl.valueChanges.subscribe((query) => {
      this.router.navigate([], {
        queryParams: { [videoListFilterQueryParam]: query },
        queryParamsHandling: 'merge',
      });
    });
  }
}
