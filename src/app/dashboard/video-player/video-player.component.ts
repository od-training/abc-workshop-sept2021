import { Component, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { Video } from '../types';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss'],
})
export class VideoPlayerComponent {
  @Input() set video(val: Video | undefined) {
    if (val) {
      const url = `https://www.youtube.com/embed/${val.id}`;
      this.trustedResourceUrl =
        this.sanitizer.bypassSecurityTrustResourceUrl(url);
    } else {
      this.trustedResourceUrl = null;
    }
  }

  trustedResourceUrl: SafeResourceUrl | null = null;

  constructor(private sanitizer: DomSanitizer) {}
}
